class Hangman 
  attr_reader :lives, :word, :unused_letters, :used_letters, :disp_word


  def initialize(word = nil)
    @lives = 3
    if word != nil
      @word = word
    else
      @word = give_word
    end
    @unused_letters = [*'a'..'z']
    @used_letters = []
    @disp_word = ['_' * (@word.length - 1)]
  end

  def guess(letter)  
    #if letter.match?(/[A-Za-z]/) && !used_letters.include?(letter) 
    #  letter.downcase
    #else 
      'Incorrect input'
    #end
  end
  
  def display(letter)
    
  end


  private
  def give_word
    words = File.open('lib/words.txt') {|f| f.readlines}
    word = words.sample.downcase 
  end


end


#new_game = Hangman.new
#puts new_game.word 
#puts new_game.disp_word
    