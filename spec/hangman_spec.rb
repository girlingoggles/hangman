require 'spec_helper'
require_relative '../lib/hangman'

RSpec.describe Hangman do
  it 'starts a game of hangman' do
    hangman = Hangman.new
    expect(hangman.used_letters).to eq []
    expect(hangman.unused_letters).to eq [*'a'..'z']
  end 

  describe '#hangman' do
    it 'returns a secret word' do
      hangman = Hangman.new
      expect(hangman.word).not_to be(nil) 
    end

    it 'gets a letter from the user' do
      hangman = Hangman.new 'potato'
      l = hangman.guess 'p'
      expect(hangman.word).to include(l)
    end

    it 'says incorrect input if incorrect input' do
      hangman = Hangman.new 'potato'
      l = hangman.guess 's'
      expect(hangman.word).not_to include(l)
      expect(hangman.guess).to eq 'Incorrect input'
    end

  end

  

end